

function getRequest(url) {
    return fetch(url)
    .then((response) =>{
        return response.json();
        })
}

function outputInfo(parent,data){
    li = document.createElement('li');
    parent.append(li);
    li.innerText = data;
}
function getInfoFilm(result){

       const { name, episodeId, openingCrawl , characters} = result;
        ul = document.createElement('ul');
        outputInfo(ul,episodeId);
        outputInfo(ul,name);
        outputInfo(ul,openingCrawl);
        const liCharacter = document.createElement('li');
        const ulCharacter = document.createElement('ul');
        ul.append(liCharacter);
        liCharacter.append(ulCharacter);
        document.body.append(ul);


    
    const loader = document.createElement('div');
    loader.innerHTML = '<i class="fa fa-spinner fa-4x fa-spin"></i>';
    ulCharacter.append(loader);

///

    const characterOutput = characters.map(characterUrl => getRequest(characterUrl));

    Promise.all(characterOutput).then(charactersFilm =>{
        ulCharacter.firstChild.remove();
        charactersFilm.forEach(character => {
            outputInfo(ulCharacter, character.name)
        })
    })
}


getRequest("https://ajax.test-danit.com/api/swapi/films")
.then((result) =>{ result.forEach(getInfoFilm)})
.catch((error) => {
    console.log("Error", error);
})




